
import requests
from requests.structures import CaseInsensitiveDict
import json 
import SQLConnections
import pandas as pd

def ApiCall(MpanList):
    url = "https://www.ecoes.co.uk/WebServices/Service/ECOESAPI.svc/RESTful/JSON/GetTechnicalDetailsByMpan"

    # Tell the Post to send as JSON 
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["Content-Type"] = "application/json"

    # Authentication Key Paramater
    AuthKey = "52NT-2EYP-UMGB-4A4X"
    AuthParam = f'''
    {{
        "Authentication":{{
            "Key":"{AuthKey}"
        }},
    '''

    print(MpanList)
	# Variables 
    try:
        for Mpan in MpanList: 
            MpanValue = Mpan

            # Paramaters for JSON POST 
            Paramaters = f'''
                "ParameterSets":[{{
                    "Parameters":[{{
                        "Key":"mpan",
                        "Value":"{MpanValue}"
                        
                    }}]
                }}]
            }}
            '''

            # Request Data  + Print the status code + change Json to Dict 
            resp = requests.post(url, data=AuthParam+Paramaters, headers=headers)
            print(resp.status_code)
            jsonn = json.loads(resp.text)

            #Querying the Json data for relevent information 
            jSonResults = jsonn['Results']

            print('break')
            print(jSonResults[0]['UtilityMatches'][0])

            Detail_List = [MpanValue] 
            for x in (jSonResults[0]['UtilityMatches'][0]['UtilityDetails'])[1:]:
                    Detail_List.append(x['Value'])


            SQLConnections.SendToSql(Detail_List)
    except: 
        with open('Failed_Mpans.txt', 'a') as f:
            f.write('\n' + MpanValue)
            print('bad mpan')
