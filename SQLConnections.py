import sys
import pypyodbc as odbc
import Loop


def ReceiveFromSql():

	#Connect to SQL Server 
	DRIVER = 'SQL Server'
	SERVER_NAME = 'PCBPG-034'
	DATABASE_NAME = 'Master'


	#Add the two below commands if not LOCALHOST 
	#Uid=BPG\Jack.Kay;
	#Pwd=CurrentPassword; 
	conn_string = f"""
		Driver={{{DRIVER}}};
		Server={SERVER_NAME};
		Database={DATABASE_NAME};
		Trust_Connection=yes;
	"""
	try:
		conn=odbc.connect(conn_string)
	except Exception as e:
		print(e)
		print('Task is Terminated')
		sys.exit()
	else: 
		cursor = conn.cursor()
		print('connected')

	Select_statement = f"""
SELECT mpan_core
FROM Mpanstwo
WHERE address_line_1 IS NULL 
AND 
address_line_2 IS NULL 
AND 
address_line_3 IS NULL 
AND 
address_line_4 IS NULL 
AND 
address_line_5 IS NULL 
AND 
address_line_6 IS NULL 
AND 
address_line_7 IS NULL 
AND 
address_line_8 IS NULL 
AND 
postcode IS NULL
;
    """

	print(Select_statement)
	MpanList = []
	try:
		cursor.execute(Select_statement)        

		for row in cursor.fetchall():
			print(row[0])
			MpanList.append(row[0])

		Loop.ApiCall(MpanList)
	
		
	finally:
		if conn.connected == 1:
			print('connection closed')
			conn.close()
			

def SendToSql(Detail_List):

	#Connect to SQL Server 
	DRIVER = 'SQL Server'
	SERVER_NAME = 'PCBPG-034'
	DATABASE_NAME = 'Master'


	#Add the two below commands if not LOCALHOST 
	#Uid=BPG\Jack.Kay;
	#Pwd=CurrentPassword; 
	conn_string = f"""
		Driver={{{DRIVER}}};
		Server={SERVER_NAME};
		Database={DATABASE_NAME};
		Trust_Connection=yes;
	"""
	try:
		conn=odbc.connect(conn_string)
	except Exception as e:
		print(e)
		print('Task is Terminated')
		sys.exit()
	else: 
		cursor = conn.cursor()
		print('connected')

	insert_statement = f"""
UPDATE Mpanstwo
SET 
address_line_1 = '{Detail_List[1]}',
address_line_2 = '{Detail_List[2]}',
address_line_3 = '{Detail_List[3]}',
address_line_4 = '{Detail_List[4]}',
address_line_5 = '{Detail_List[5]}',
address_line_6 = '{Detail_List[6]}',
address_line_7 = '{Detail_List[7]}',
address_line_8 = '{Detail_List[8]}',
address_line_9 = '{Detail_List[9]}',
postcode = '{Detail_List[10]}',
trading_status_efd = '{Detail_List[11]}',
profile_class = '{Detail_List[12]}',
profile_class_efd = '{Detail_List[13]}',
meter_timeswitch_class = '{Detail_List[14]}',
meter_timeswitch_class_efd = '{Detail_List[15]}',
line_loss_factor = '{Detail_List[16]}',
line_loss_factor_efd = '{Detail_List[17]}',
standard_settlement_configuration = '{Detail_List[18]}',
standard_settlement_configuration_efd = '{Detail_List[19]}',
energisation_status = '{Detail_List[20]}',
trading_status = '{Detail_List[21]}',
gsp_group_id = '{Detail_List[22]}',
gsp_group_efd = '{Detail_List[23]}',
data_aggregator_mpid = '{Detail_List[24]}',
data_aggregator_efd = '{Detail_List[25]}',
data_collector_mpid = '{Detail_List[26]}',
data_collector_efd = '{Detail_List[27]}',
supplier_mpid = '{Detail_List[28]}',
supplier_efd = '{Detail_List[29]}',
energisation_status_efd = '{Detail_List[30]}',
meter_operator_mpid = '{Detail_List[31]}',
meter_operator_efd = '{Detail_List[32]}',
measurement_class = '{Detail_List[33]}',
measurement_class_efd = '{Detail_List[34]}',
green_deal_in_effect = '{Detail_List[35]}',
smso_mpid = '{Detail_List[36]}', 
smso_efd = '{Detail_List[37]}',
dcc_service_flag = '{Detail_List[38]}',
dcc_service_flag_efd = '{Detail_List[39]}',
ihd_status = '{Detail_List[40]}',
ihd_status_efd = '{Detail_List[41]}',
smets_version = '{Detail_List[42]}',
distributor_mpid = '{Detail_List[43]}',
metered_indicator = '{Detail_List[44]}',
metered_indicator_efd = '{Detail_List[45]}',
metered_indicator_etd = '{Detail_List[46]}',
consumer_type = '{Detail_List[47]}',
domestic_consumer_indicator = '{Detail_List[48]}',
relationship_status_indicator = '{Detail_List[49]}',
rmp_state = '{Detail_List[50]}',
rmp_efd = '{Detail_List[51]}'
				
WHERE mpan_core = '{Detail_List[0]}';
    """

	print(insert_statement)


	try:
		cursor.execute(insert_statement)        
	except Exception as e:
		cursor.rollback()
		print(e)
		print(e.value)
		print('transaction rolled back')
	else:
		print('records inserted successfully')
		cursor.commit()
		cursor.close()
	finally:
		if conn.connected == 1:
			print('connection closed')
			conn.close()